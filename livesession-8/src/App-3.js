import { useState } from "react";
import "./App.css";
import Header from "./components/Header";

function App() {
  let fullname = "Farhan";
  const display = false;
  const displayAnd = false;
  const [name, setName] = useState("Abdul Hamid");

  const changeText = () => {
    // name = "Rayhan";
    setName("Rayhan");
  };

  return (
    <div>
      <Header fullname={fullname} state={name} changeText={changeText} />

      {display === true ? (
        <h1 className="text-4xl">Hello World Terpenuhi</h1>
      ) : (
        <h1 className="text-4xl">Hello World Tidak Terpenuhi</h1>
      )}

      {displayAnd === true && (
        <h1 className="text-3xl text-red-500">Kondisi terpenuhi</h1>
      )}
    </div>
  );
}

export default App;
