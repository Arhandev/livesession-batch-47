import { useState } from "react";
import "./App.css";

function App() {
  // let name = "Farhan Abdul Hamid";
  const [name, setName] = useState(["abc", "def"]);

  const changeText = () => {
    // name = "Rayhan";
    setName("Rayhan");
  };

  return (
    <div>
      <p className="text-xl text-blue-700">{name}</p>

      <button
        onClick={changeText}
        className="bg-blue-800 p-6 text-white text-xl"
      >
        Klik aku
      </button>
    </div>
  );
}

export default App;
