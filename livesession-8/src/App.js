import { useEffect, useState } from "react";
import "./App.css";
import Header from "./components/Header";

function App() {
  const [name, setName] = useState("Abdul Hamid");
  const [isDisplay, setIsDisplay] = useState(true);

  const changeText = () => {
    // name = "Rayhan";
    setName("Rayhan");
  };

  const changeDisplay = () => {
    setIsDisplay(!isDisplay); // merubah value yang berlawanan dengan value yang sekarang
  };

  // mounting
  // useEffect(() => {
  //   console.log("Mounting life cycle berjalan");
  //   console.log("Melakukan fetching data");
  //   //fetching data
  // }, []);

  // update lifecycle
  // event update lifecycle ini bakal berjalan juga ketika
  // component pertama kali mounting
  // useEffect(() => {
  //   console.log("Terjadi perubahan");
  // }, [name]);

  return (
    <div>
      <h1 className="text-4xl text-red-600">Hello World</h1>

      <p className="text-5xl text-blue-500">{name}</p>
      <button
        onClick={changeText}
        className="bg-blue-800 p-6 text-white text-xl"
      >
        Klik aku
      </button>

      <button
        onClick={changeDisplay}
        className="bg-red-700 p-6 text-white text-xl mt-20 block"
      >
        Show/hide Header
      </button>

      {isDisplay === true && <Header />}
    </div>
  );
}

export default App;
