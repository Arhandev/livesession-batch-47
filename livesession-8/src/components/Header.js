import React, { useEffect } from "react";

function Header(props) {
  useEffect(() => {
    // console.log("Mounting life cycle dari Header");
    //fetching data
    console.log("Mengalami Mounting"); // barisan ini hanya berjalan ketika mounting
    return () => {
      console.log("Mengalami Unmounting"); // barisan ini hanya berjalan ketika unmounting
    };
  }, []);
  return (
    <div>
      <h1 className="header">Ini dari Header Component</h1>
      {/* <p className="text-5xl text-green-500">{props.fullname}</p>
      <p className="text-5xl text-blue-500">{props.state}</p> */}

      {/* <button
        onClick={props.changeText}
        className="bg-blue-800 p-6 text-white text-xl"
      >
        Klik aku
      </button> */}
    </div>
  );
}

export default Header;
