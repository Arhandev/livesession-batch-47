import "./App.css";
import style from "./App.module.css";
import Header from "./components/Header";


// PARENT COMPONENT
function App() {
  const name = "Farhan Abdul Hamid";
  const array = ["Nama", "saya", "Farhan"];

  const popAlert = () => {
    alert("Halo Dunia");
  };

  return (
    <div>
      <h1 className={style.header}>Hello World</h1>
      <p className="text-xl text-blue-700">{`My name is ${name}`}</p>
      {array.map((item) => {
        return <p className="text-4xl">{item}</p>;
      })}
      <p className="text-4xl">Nama</p>
      <p className="text-4xl">saya</p>
      <p className="text-4xl">Farhan</p>

      {/* CHILD COMPONENT */}
      <Header /> 

      <button onClick={popAlert} className="bg-blue-800 p-6 text-white text-xl">
        Klik aku
      </button>
      <input
        type="text"
        placeholder="masukan nama"
        className="border-2 border-gray-500"
      />
    </div>
  );
}

export default App;
