import axios from "axios";
import { useEffect, useState } from "react";
import "./App.css";
import Article from "./components/Article";
import CreateForm from "./components/CreateForm";

function App() {
  const [articles, setArticles] = useState([]);

  const fetchArticles = async () => {
    try {
      // fetch data menggunakan axios
      const response = await axios.get(
        "https://api-project.amandemy.co.id/api/articles"
      );
      // console.log(response.data.data); // berhasil mengakses isi artikel
      setArticles(response.data.data); // berhasil menyimpan artikel pada state
      // articles sudah terisi data dari server
    } catch (error) {
      alert("Terjadi Sesuatu Error");
    }
  };

  useEffect(() => {
    // console.log("Fetch Data.....");
    fetchArticles();
  }, []);

  return (
    <div className="">
      {/* <FormBinding /> */}
      <div className="max-w-5xl mx-auto my-8">
        <CreateForm fetchArticles={fetchArticles} />
        <h1 className="text-3xl font-bold text-center">Articles</h1>
        <div className="flex flex-col gap-6">
          {articles.map((article) => (
            <Article article={article} />
          ))}
        </div>
      </div>
    </div>
  );
}

export default App;
