import axios from "axios";
import React, { useState } from "react";

function CreateForm({ fetchArticles }) {
  const [input, setInput] = useState({
    name: "", // nama barang
    content: "", // harga barang
    image_url: "", // deskripsi barang
    highlight: false,
  });

  const handleChange = (event) => {
    // jika si inputnya dari elemen nama maka set key nama
    // jika si inputnya dari elemen harga maka set key harga

    // console.log(event.target.name);

    if (event.target.name === "name") {
      setInput({
        ...input,
        name: event.target.value,
      });
    } else if (event.target.name === "content") {
      setInput({
        ...input,
        content: event.target.value,
      });
    } else if (event.target.name === "image_url") {
      setInput({
        ...input,
        image_url: event.target.value,
      });
    } else if (event.target.name === "highlight") {
      setInput({
        ...input,
        highlight: event.target.checked, // checkbox
      });
    }
  };

  const handleSubmit = async () => {
    // console.log(input);
    console.log("Menjalankan Post Request");
    try {
      // fetch data menggunakan axios
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/articles",
        {
          name: input.name,
          content: input.content,
          image_url: input.image_url,
          highlight: input.highlight,
        }
      );
      // console.log(response.data.data);
      fetchArticles();
      setInput({
        name: "", // nama barang
        content: "", // harga barang
        image_url: "", // deskripsi barang
        highlight: false,
      });
      alert("Artikel berhasil dibuat");
    } catch (error) {
      alert("Terjadi Sesuatu Error");
    }
  };

  return (
    <div>
      <section className="max-w-6xl mx-auto shadow-lg rounded-lg bg-white p-8 my-8">
        <h1 className="text-2xl text-cyan-500">Create Article</h1>
        <div className="grid grid-cols-5 gap-x-6 gap-y-4 my-2">
          <div className="col-span-5">
            <label className="block mb-1"> Nama Article</label>
            <input
              onChange={handleChange}
              name="name"
              type="text"
              className="block border border-gray-400 rounded-md w-full px-2 py-1 text-sm"
              placeholder="Masukkan Nama Article"
              value={input.name}
            />
          </div>

          <div className="col-span-5">
            <label className="block mb-1"> Konten Artikel</label>
            <textarea
              onChange={handleChange}
              name="content"
              type="text"
              className="block border border-gray-400 rounded-md w-full px-2 py-1 text-sm"
              placeholder="Masukkan Deskripsi Artikel"
              value={input.content}
            ></textarea>
          </div>

          <div className="col-span-5">
            <label className="block mb-1">Image URL</label>
            <input
              onChange={handleChange}
              name="image_url"
              type="text"
              className="block border border-gray-400 rounded-md w-full px-2 py-1 text-sm"
              placeholder="Masukkan Image URL"
              value={input.image_url}
            />
          </div>
          <div className="flex items-center justify-center mt-4 gap-3">
            <input
              onChange={handleChange}
              name="highlight"
              type="checkbox"
              className=""
              checked={input.highlight}
            />
            <label className="">Is Highlight?</label>
          </div>
        </div>
        <div className="flex justify-end gap-4 my-4">
          <button className="text-cyan-500 border-2 border-cyan-500 px-4 py-1 rounded-lg">
            Cancel
          </button>
          <button
            onClick={handleSubmit}
            className="bg-cyan-500 text-white border-2 border-cyan-500 px-4 py-1 rounded-lg"
          >
            Submit
          </button>
        </div>
      </section>
    </div>
  );
}

export default CreateForm;
