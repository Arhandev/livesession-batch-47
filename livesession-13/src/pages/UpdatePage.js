import React from "react";
import CreateForm from "../components/CreateForm";
import Navbar from "../components/Navbar";
import UpdateForm from "../components/UpdateForm";

function UpdatePage() {
  return (
    <div>
      {/* NAVBAR */}
      <Navbar />
      <UpdateForm/>
    </div>
  );
}

export default UpdatePage;
