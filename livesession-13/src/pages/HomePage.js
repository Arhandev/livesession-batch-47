import React, { useContext, useEffect } from "react";
import Article from "../components/Article";
import Navbar from "../components/Navbar";
import { GlobalContext } from "../context/GlobalContext";

function HomePage() {
  const { articles, fetchArticles, loading } = useContext(GlobalContext);

    useEffect(() => {
      // console.log("Fetch Data.....");
      fetchArticles();
    }, []);


  return (
    <div>
      {/* NAVBAR */}
      <Navbar />
      <div className="max-w-5xl mx-auto my-10">
        <h1 className="text-3xl font-bold text-center mb-4">Articles</h1>
        <div className="flex flex-col gap-6">
          {articles.map((article, index) => (
            <Article key={article.id} article={article} />
          ))}
        </div>
      </div>
    </div>
  );
}

export default HomePage;
