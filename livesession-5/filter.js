const arr = [5, 18, 12, 8, 15, 20, 30];

// sebuah array yang isi nya value dari arr dengan kondisi value nya lebih dari 10
const filterArr = arr.filter((item, index) => {
  // kondisi harus category dari pilihan user
  return item > 10;
});

console.log(filterArr);
