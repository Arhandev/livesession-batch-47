const arr1 = [1, 2, 3, 4, 5];
const arr2 = [6, 7, 8, 9];

const arr3 = [...arr2, ...arr1];

console.log(arr3);

// const obj1 = {
//   nama: "Farhan",
//   domisili: "jakarta",
// };

// const obj2 = {
//   nama: "Rayhan",
//   alamat: "Jalan Mawar",
// };

// const obj3 = {
//   ...obj2,
//   ...obj1,
// };

// console.log(obj3);
