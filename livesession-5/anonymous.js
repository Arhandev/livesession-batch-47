const printAnonymous = (params) => {
  console.log("Function Print Anonymous berjalan");
  params();
  console.log("=====================================");
};

printAnonymous(() => {
  console.log("Anonymous Function pertama");
});

printAnonymous(() => {
  console.log("Anonymous Function Kedua");
  console.log("Halo Dunia");
});
