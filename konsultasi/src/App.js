import axios from "axios";
import { useEffect, useState } from "react";
import "./App.css";
import Article from "./components/Article";
import CreateForm from "./components/CreateForm";
import Table from "./components/Table";
import UpdateForm from "./components/UpdateForm";

function App() {
  const [articles, setArticles] = useState([]);
  const [editArticle, setEditArticle] = useState(null); // mendapatkan akses data pada artikel yang hendak kita edit
  // Isi dari editArticle
  /*
  {
    "id": 185,
    "name": "Artikel Live Session 10",
    "content": "Ini percobaan pertama live session 10 Post",
    "image_url": "http://api-project.amandemy.co.id//images/pisang.jpg",
    "highlight": true,
    "created_at": "2023-08-06T14:04:09.000000Z",
    "updated_at": "2023-08-06T14:04:09.000000Z",
    "user_id": null,
    "user": null
  }
  */

  const fetchArticles = async () => {
    try {
      // fetch data menggunakan axios
      const response = await axios.get(
        "https://api-project.amandemy.co.id/api/articles"
      );
      // console.log(response.data.data); // berhasil mengakses isi artikel
      setArticles(response.data.data); // berhasil menyimpan artikel pada state
      // articles sudah terisi data dari server
    } catch (error) {
      alert("Terjadi Sesuatu Error");
    }
  };

  useEffect(() => {
    // console.log("Fetch Data.....");
    fetchArticles();
  }, []);

  return (
    <div className="">
      {/* <FormBinding /> */}
      <div className="max-w-6xl mx-auto my-8">
        {editArticle === null ? (
          <CreateForm fetchArticles={fetchArticles} />
        ) : (
          <UpdateForm
            editArticle={editArticle}
            setEditArticle={setEditArticle}
            fetchArticles={fetchArticles}
          />
        )}

        <Table
          setEditArticle={setEditArticle}
          fetchArticles={fetchArticles}
          articles={articles}
        />
        <h1 className="text-3xl font-bold text-center">Articles</h1>
        <div className="flex flex-col gap-6">
          {articles.map((article, index) => (
            <Article key={article.id} article={article} />
          ))}
        </div>
      </div>
    </div>
  );
}

export default App;
