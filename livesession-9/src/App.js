import { useState } from "react";
import "./App.css";

function App() {
  const [input, setInput] = useState({
    nama: "", // nama barang
    harga: 0, // harga barang
    deskripsi: "", // deskripsi barang
    is_highlight: false,
  });

  const handleChange = (event) => {
    // jika si inputnya dari elemen nama maka set key nama
    // jika si inputnya dari elemen harga maka set key harga

    // console.log(event.target.name);

    if (event.target.name === "nama") {
      setInput({
        ...input,
        nama: event.target.value,
      });
    } else if (event.target.name === "harga") {
      setInput({
        ...input,
        harga: event.target.value,
      });
    } else if (event.target.name === "deskripsi") {
      setInput({
        ...input,
        deskripsi: event.target.value,
      });
    } else if (event.target.name === "is_highlight") {
      setInput({
        ...input,
        is_highlight: event.target.checked, // checkbox
      });
    }
  };

  const handleSubmit = () => {
    console.log(input);
  };

  return (
    <div className="">
      <section className="max-w-6xl mx-auto shadow-lg rounded-lg bg-white p-8 my-8">
        <h1 className="text-2xl text-cyan-500">Create Product</h1>
        <div className="grid grid-cols-5 gap-x-6 gap-y-4 my-2">
          <div className="col-span-5">
            <label className="block mb-1"> Nama Barang</label>
            <input
              onChange={handleChange}
              name="nama"
              type="text"
              className="block border border-gray-400 rounded-md w-full px-2 py-1 text-sm"
              placeholder="Masukkan Nama Barang"
            />
          </div>
          <div className="col-span-5">
            <label className="block mb-1"> Harga Barang</label>
            <input
              onChange={handleChange}
              name="harga"
              type="number"
              className="block border border-gray-400 rounded-md w-full px-2 py-1 text-sm"
              placeholder="Masukkan Harga Barang"
            />
          </div>
          <div className="col-span-5">
            <label className="block mb-1"> Deskripsi Barang</label>
            <textarea
              onChange={handleChange}
              name="deskripsi"
              type="text"
              className="block border border-gray-400 rounded-md w-full px-2 py-1 text-sm"
              placeholder="Masukkan Deskripsi Barang"
            ></textarea>
          </div>
          <div className="flex items-center justify-center mt-4 gap-3">
            <input
              onChange={handleChange}
              name="is_highlight"
              type="checkbox"
              className=""
            />
            <label className="">Is Highlight?</label>
          </div>
        </div>
        <div className="flex justify-end gap-4 my-4">
          <button className="text-cyan-500 border-2 border-cyan-500 px-4 py-1 rounded-lg">
            Cancel
          </button>
          <button
            onClick={handleSubmit}
            className="bg-cyan-500 text-white border-2 border-cyan-500 px-4 py-1 rounded-lg"
          >
            Submit
          </button>
        </div>
      </section>
    </div>
  );
}

export default App;
