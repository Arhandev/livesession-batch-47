import React, { useContext, useEffect } from "react";
import { Helmet } from "react-helmet";
import Table from "../components/Table";
import { GlobalContext } from "../context/GlobalContext";
import Layout from "../layout/Layout";

function TablePage() {
  const { articles, fetchArticles, loading } = useContext(GlobalContext);

  useEffect(() => {
    // console.log("Fetch Data.....");
    fetchArticles();
  }, []);

  return (
    <div>
      {/* NAVBAR */}
      <Helmet>
        <title>Table Article Page</title>
      </Helmet>
      <Layout>
        <Table />
      </Layout>
    </div>
  );
}

export default TablePage;
