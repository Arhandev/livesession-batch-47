import React from "react";
import UpdateForm from "../components/UpdateForm";
import ProfileLayout from "../layout/ProfileLayout";

function UpdatePage() {
  return (
    <div>
      {/* NAVBAR */}

      <ProfileLayout>
        <UpdateForm />
      </ProfileLayout>
    </div>
  );
}

export default UpdatePage;
