import React from "react";
import { Helmet } from "react-helmet";
import CreateForm from "../components/CreateForm";
import ProfileLayout from "../layout/ProfileLayout";

function CreatePage() {
  return (
    <div>
      {/* NAVBAR */}
      <Helmet>
        <title>Create Article Page</title>
      </Helmet>
      <ProfileLayout>
        <CreateForm />
      </ProfileLayout>
    </div>
  );
}

export default CreatePage;
