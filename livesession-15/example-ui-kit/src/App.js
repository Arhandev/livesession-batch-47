import { Button, Dropdown } from "antd";
import "./App.css";

function App() {
  return (
    <div>
      <div className="w-64 bg-red-400 text-green-700">
        <Dropdown
          menu={{
            items: [
              {
                key: "menu-1",
                label: "Menu 1",
              },
              {
                key: "menu-2",
                label: "Menu 2",
              },
              {
                key: "menu-3",
                label: "Menu 3",
              },
              {
                key: "menu-4",
                label: "Menu 4",
              },
            ],
          }}
          trigger={["click"]}
        >
          <h1>Hover Aku</h1>
        </Dropdown>
      </div>
      <div>
        <Button type="primary">Klik Aku</Button>
      </div>
    </div>
  );
}

export default App;
