import React from "react";
import Footer from "../components/Footer";
import Navbar from "../components/Navbar";

function ProfileLayout({ children }) {
  return (
    <div>
      <Footer />
      {/* <Navbar />
      <Navbar /> */}
      {children}
      <Navbar />
    </div>
  );
}

export default ProfileLayout;
