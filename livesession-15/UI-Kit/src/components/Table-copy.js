import { Button } from "antd";
import axios from "axios";
import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { GlobalContext } from "../context/GlobalContext";

function Table() {
  const dataSource = [];
  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
    },
    {
      title: "Nama Artikel",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Konten Artikel",
      dataIndex: "content",
      key: "content",
    },
    {
      title: "Image URL",
      dataIndex: "image_url",
      key: "image_url",
    },
    {
      title: "Highlight",
      dataIndex: "highlight",
      key: "highlight",
    },
    {
      title: "Action",
      dataIndex: "action",
      key: "action",
    },
  ];

  const { articles, fetchArticles } = useContext(GlobalContext);

  const onDelete = async (id) => {
    try {
      // mengirimkan sebuah delete request
      const response = await axios.delete(
        `https://api-project.amandemy.co.id/api/articles/${id}`
      );
      fetchArticles();
      alert("Berhasil menghapus artikel");
    } catch (error) {
      alert("Terjadi Sesuatu Error");
    }
  };
  return (
    <section>
      <h1 className="my-8 text-3xl font-bold text-center">Table Article</h1>
      <div className="max-w-6xl mx-auto w-full my-4">
        <table className="border border-gray-500 border-solid w-full">
          <thead>
            <tr>
              <th className="border border-gray-500 border-solid p-2">ID</th>
              <th className="border border-gray-500 border-solid p-2">Name</th>
              <th className="border border-gray-500 border-solid p-2">
                Content
              </th>
              <th className="border border-gray-500 border-solid p-2">Image</th>
              <th className="border border-gray-500 border-solid p-2">
                Highlight
              </th>
              <th className="border border-gray-500 border-solid p-2">
                Action
              </th>
            </tr>
          </thead>
          <tbody>
            {articles.map((article, index) => {
              return (
                <tr key={article.id}>
                  <td className="border border-gray-500 border-solid p-2">
                    {article.id}
                  </td>
                  <td className="border border-gray-500 border-solid p-2">
                    {article.name}
                  </td>
                  <td className="border border-gray-500 border-solid p-2">
                    {article.content}
                  </td>
                  <td className="border border-gray-500 border-solid p-2">
                    <img src={article.image_url} alt="" className="w-64" />
                  </td>
                  <td className="border border-gray-500 border-solid p-2">
                    {article.highlight === true ? "Aktif" : "Tidak Aktif"}
                  </td>

                  <td className="border border-gray-500 border-solid p-2">
                    <div className="flex gap-2">
                      <Link to={`/update/${article.id}`}>
                        <Button type="primary">Update</Button>
                      </Link>
                      <Button
                        type="primary"
                        onClick={() => {
                          //   console.log(article.id);
                          onDelete(article.id);
                        }}
                        danger
                      >
                        Delete
                      </Button>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </section>
  );
}

export default Table;
