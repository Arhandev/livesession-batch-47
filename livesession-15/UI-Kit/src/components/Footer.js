import React from "react";

function Footer() {
  return (
    <div className="text-white text-center text-3xl bg-blue-600 py-5">
      Copyright Farhan Abdul Hamid
    </div>
  );
}

export default Footer;
