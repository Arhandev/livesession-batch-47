import React from "react";
import { Link } from "react-router-dom";

function Navbar() {
  return (
    <header class="shadow-lg py-4 bg-white px-12 sticky z-10 top-0">
      <nav class="mx-auto max-w-7xl flex justify-between">
        <div></div>
        <ul class="flex items-center gap-6 text-xl list-none my-0">
          <Link className="text-cyan-500 no-underline" to="/">
            <li>Home</li>
          </Link>
          <Link className="text-cyan-500 no-underline" to="/table">
            <li>Table</li>
          </Link>
          <Link className="text-cyan-500 no-underline" to="/create">
            <li>Form</li>
          </Link>
        </ul>
        <div class="flex items-center gap-4"></div>
      </nav>
    </header>
  );
}

export default Navbar;
