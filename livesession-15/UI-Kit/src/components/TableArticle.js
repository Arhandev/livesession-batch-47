import { Button, Table } from "antd";
import axios from "axios";
import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { GlobalContext } from "../context/GlobalContext";

function TableArticle() {
  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
    },
    {
      title: "Nama Artikel",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Konten Artikel",
      dataIndex: "content",
      key: "content",
    },
    {
      title: "Image URL",
      dataIndex: "image_url",
      key: "image_url",
      render: (text, record, index) => {
        return <img className="w-64" src={record.image_url} alt="" />;
      },
    },
    {
      title: "Highlight",
      dataIndex: "highlight",
      key: "highlight",
      render: (text, record, index) => {
        if (record.highlight === true) {
          return <p>Aktif</p>;
        } else {
          return <p>Tidak Aktif</p>;
        }
      },
    },
    {
      title: "Action",
      dataIndex: "action",
      key: "action",
      render: (text, record, index) => {
        return (
          <div className="flex gap-2">
            <Link to={`/update/${record.id}`}>
              <Button type="primary">Update</Button>
            </Link>
            <Button
              type="primary"
              onClick={() => {
                //   console.log(record.id);
                onDelete(record.id);
              }}
              danger
            >
              Delete
            </Button>
          </div>
        );
      },
    },
  ];

  const { articles, fetchArticles } = useContext(GlobalContext);

  const onDelete = async (id) => {
    try {
      // mengirimkan sebuah delete request
      const response = await axios.delete(
        `https://api-project.amandemy.co.id/api/articles/${id}`
      );
      fetchArticles();
      alert("Berhasil menghapus artikel");
    } catch (error) {
      alert("Terjadi Sesuatu Error");
    }
  };
  return (
    <section>
      <h1 className="my-8 text-3xl font-bold text-center">Table Article</h1>
      <div className="max-w-6xl mx-auto w-full my-4">
        <Table dataSource={articles} columns={columns} />
      </div>
    </section>
  );
}

export default TableArticle;
