import { Button, Checkbox, Input } from "antd";
import axios from "axios";
import { useFormik } from "formik";
import React, { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import * as Yup from "yup";
import { GlobalContext } from "../context/GlobalContext";

const validationSchema = Yup.object({
  name: Yup.number()
    .typeError("Masukan angka yang valid")
    .required("Nama Artikel wajib diisi"),
  content: Yup.string().required("Konten Artikel wajib diisi"),
  image_url: Yup.string()
    .required("Url Gambar Artikel wajib diisi")
    .url("Format URL tidak valid"),
  highlight: Yup.boolean(),
  catatan_highlight: Yup.string().when("highlight", {
    is: true,
    then: Yup.string().required("Catatan Highlight wajib diisi"),
  }),
});

function CreateForm() {
  const { fetchArticles } = useContext(GlobalContext);
  const navigate = useNavigate();
  const [input, setInput] = useState({
    name: "", // nama barang
    content: "", // harga barang
    image_url: "", // deskripsi barang
    highlight: false,
    catatan_highlight: "",
    test: "",
  });

  const onSubmit = async (values) => {
    console.log(values);
    console.log("Menjalankan Post Request");
    try {
      // fetch data menggunakan axios
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/articles",
        {
          name: values.name,
          content: values.content,
          image_url: values.image_url,
          highlight: values.highlight,
        }
      );
      // console.log(response.data.data);
      fetchArticles();
      setInput({
        name: "", // nama barang
        content: "", // harga barang
        image_url: "", // deskripsi barang
        highlight: false,
        test: "c-1",
      });
      alert("Artikel berhasil dibuat");
      navigate("/table");
    } catch (e) {
      // console.log(e.response.data.info);
      alert(e.response.data.info);
    }
  };

  const {
    handleChange,
    values,
    handleSubmit,
    errors,
    touched,
    handleBlur,
    setFieldValue,
    setFieldTouched,
  } = useFormik({
    initialValues: input,
    onSubmit: onSubmit,
    validationSchema: validationSchema,
  });

  return (
    <div>
      <section className="max-w-6xl mx-auto shadow-lg rounded-lg bg-white p-8 my-8">
        <h1 className="text-2xl text-cyan-500">Create Article</h1>
        <div className="grid grid-cols-5 gap-x-6 gap-y-4 my-2">
          <div className="col-span-5">
            <label className="block mb-1"> Nama Article</label>
            <Input
              onChange={handleChange}
              name="name"
              type="text"
              className="block border border-gray-400 rounded-md w-full px-2 py-1 text-sm"
              placeholder="Masukkan Nama Article"
              value={values.name}
              onBlur={handleBlur}
            />
            {touched.name === true && errors.name != null && (
              <p className="my-0 text-red-500 text-lg">{errors.name}</p>
            )}
          </div>

          <div className="col-span-5">
            <label className="block mb-1"> Konten Artikel</label>
            <Input.TextArea
              onChange={handleChange}
              name="content"
              type="text"
              className="block border border-gray-400 rounded-md w-full px-2 py-1 text-sm"
              placeholder="Masukkan Deskripsi Artikel"
              value={values.content}
              onBlur={handleBlur}
            ></Input.TextArea>
            {touched.content === true && errors.content != null && (
              <p className="my-0 text-red-500 text-lg">{errors.content}</p>
            )}
          </div>

          <div className="col-span-5">
            <label className="block mb-1">Image URL</label>
            <Input
              onChange={handleChange}
              name="image_url"
              type="text"
              className="block border border-gray-400 rounded-md w-full px-2 py-1 text-sm"
              placeholder="Masukkan Image URL"
              value={values.image_url}
              onBlur={handleBlur}
            />
            {touched.image_url === true && errors.image_url != null && (
              <p className="my-0 text-red-500 text-lg">{errors.image_url}</p>
            )}
          </div>
          <div className="col-span-5">
            <label className="block mb-1">Select Type</label>
            <select
              id=""
              onChange={(event) => setFieldValue("test", event.target.value)}
              onBlur={() => setFieldTouched("test")}
              name="test"
              value={values.test}
            >
              <option value="" disabled>
                Pilihlah jawaban dibawah
              </option>
              <option value="c-1">Pilihan 1</option>
              <option value="c-2">Pilihan 2</option>
              <option value="c-3">Pilihan 3</option>
            </select>
          </div>
          <div className="flex items-center justify-center mt-4 gap-3">
            <Checkbox
              onChange={(event) =>
                setFieldValue("highlight", event.target.checked)
              }
              onBlur={() => setFieldTouched("highlight")}
              name="highlight"
              type="checkbox"
              className=""
              checked={values.highlight}
            />

            <label className="">Is Highlight?</label>
          </div>

          {values.highlight === true && (
            <div className="col-span-5">
              <label className="block mb-1">Catatan Highlight</label>
              <Input
                onChange={handleChange}
                name="catatan_highlight"
                type="text"
                className="block border border-gray-400 rounded-md w-full px-2 py-1 text-sm"
                placeholder="Masukkan Image URL"
                value={values.catatan_highlight}
                onBlur={handleBlur}
              />
              {touched.catatan_highlight === true &&
                errors.catatan_highlight != null && (
                  <p className="my-0 text-red-500 text-lg">
                    {errors.catatan_highlight}
                  </p>
                )}
            </div>
          )}
        </div>
        <div className="flex justify-end gap-4 my-4">
          <Button>Cancel</Button>
          <Button htmlType="submit" type="primary" onClick={handleSubmit}>
            Submit
          </Button>
        </div>
      </section>
    </div>
  );
}

export default CreateForm;
