import axios from "axios";
import { useFormik } from "formik";
import React, { useContext, useEffect, useState } from "react";
import { Helmet } from "react-helmet";
import { useNavigate, useParams } from "react-router-dom";
import * as Yup from "yup";
import { GlobalContext } from "../context/GlobalContext";

const validationSchema = Yup.object({
  name: Yup.string().required("Nama Artikel wajib diisi"),
  content: Yup.string().required("Konten Artikel wajib diisi"),
  image_url: Yup.string()
    .required("Url Gambar Artikel wajib diisi")
    .url("Format URL tidak valid"),
  highlight: Yup.boolean(),
});

function UpdateForm() {
  const { fetchArticles } = useContext(GlobalContext);
  const { articleId } = useParams();
  const navigate = useNavigate();
  const [input, setInput] = useState({
    name: "", // nama barang
    content: "", // harga barang
    image_url: "", // deskripsi barang
    highlight: false,
  });

  const onSubmit = async (values) => {
    // console.log(input);
    console.log("Menjalankan Put Request");
    try {
      // fetch data menggunakan axios
      const response = await axios.put(
        `https://api-project.amandemy.co.id/api/articles/${articleId}`,
        {
          name: values.name,
          content: values.content,
          image_url: values.image_url,
          highlight: values.highlight,
        }
      );
      // console.log(response.data.data);
      fetchArticles();
      setInput({
        name: "", // nama barang
        content: "", // harga barang
        image_url: "", // deskripsi barang
        highlight: false,
      });
      // mereset kembali edit articles
      alert("Artikel berhasil diubah");
      navigate("/table");
    } catch (e) {
      // console.log(e.response.data.info);
      alert(e.response.data.info);
    }
  };

  // melakukan fetch article detail
  const fetchArticleDetail = async () => {
    try {
      // fetch data menggunakan axios
      const response = await axios.get(
        `https://api-project.amandemy.co.id/api/articles/${articleId}`
      );
      console.log(response.data.data);
      const article = response.data.data;
      // kita memasukkan data dari server ke dalam form nya
      setInput({
        name: article.name,
        content: article.content,
        image_url: article.image_url,
        highlight: article.highlight,
      });
    } catch (error) {
      alert("Terjadi Sesuatu Error");
    }
  };

  const {
    handleChange,
    values,
    handleSubmit,
    errors,
    touched,
    handleBlur,
    setFieldValue,
    setFieldTouched,
  } = useFormik({
    initialValues: input,
    onSubmit: onSubmit,
    validationSchema: validationSchema,
    enableReinitialize: true,
  });

  useEffect(() => {
    fetchArticleDetail();
  }, []);

  return (
    <div>
      <Helmet>
        <title>Update Article ID: {articleId}</title>
      </Helmet>
      <section className="max-w-6xl mx-auto shadow-lg rounded-lg bg-white p-8 my-8">
        <h1 className="text-2xl text-cyan-500">Update Article</h1>
        <div className="grid grid-cols-5 gap-x-6 gap-y-4 my-2">
          <div className="col-span-5">
            <label className="block mb-1"> Nama Article</label>
            <input
              onChange={handleChange}
              name="name"
              type="text"
              className="block border border-gray-400 rounded-md w-full px-2 py-1 text-sm"
              placeholder="Masukkan Nama Article"
              value={values.name}
              onBlur={handleBlur}
            />
            {touched.name === true && errors.name != null && (
              <p className="text-red-500 text-lg">{errors.name}</p>
            )}
          </div>

          <div className="col-span-5">
            <label className="block mb-1"> Konten Artikel</label>
            <textarea
              onChange={handleChange}
              name="content"
              type="text"
              className="block border border-gray-400 rounded-md w-full px-2 py-1 text-sm"
              placeholder="Masukkan Deskripsi Artikel"
              value={values.content}
              onBlur={handleBlur}
            ></textarea>
            {touched.content === true && errors.content != null && (
              <p className="text-red-500 text-lg">{errors.content}</p>
            )}
          </div>

          <div className="col-span-5">
            <label className="block mb-1">Image URL</label>
            <input
              onChange={handleChange}
              name="image_url"
              type="text"
              className="block border border-gray-400 rounded-md w-full px-2 py-1 text-sm"
              placeholder="Masukkan Image URL"
              value={values.image_url}
              onBlur={handleBlur}
            />
            {touched.image_url === true && errors.image_url != null && (
              <p className="text-red-500 text-lg">{errors.image_url}</p>
            )}
          </div>
          <div className="flex items-center justify-center mt-4 gap-3">
            <input
              onChange={(event) =>
                setFieldValue("highlight", event.target.checked)
              }
              onBlur={() => setFieldTouched("highlight")}
              name="highlight"
              type="checkbox"
              className=""
              checked={values.highlight}
            />
            <label className="">Is Highlight?</label>
          </div>
        </div>
        <div className="flex justify-end gap-4 my-4">
          <button className="text-cyan-500 border-2 border-cyan-500 px-4 py-1 rounded-lg">
            Cancel
          </button>
          <button
            onClick={handleSubmit}
            className="bg-cyan-500 text-white border-2 border-cyan-500 px-4 py-1 rounded-lg"
          >
            Submit
          </button>
        </div>
      </section>
    </div>
  );
}

export default UpdateForm;
