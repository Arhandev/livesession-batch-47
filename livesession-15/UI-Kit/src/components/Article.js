import React from "react";

function Article({ article }) {
  return (
    <div className="border-2 border-black border-solid p-6 flex gap-8">
      <div className="w-64">
        <img
          src={article.image_url}
          className="w-64 h-64 object-cover"
          alt=""
        />
      </div>
      <div>
        <h1 className="font-bold text-2xl">{article.name}</h1>
        <p className="mt-4 text-lg">{article.content}</p>
      </div>
    </div>
  );
}

export default Article;
