import React from "react";
import WrapperComponent from "../components/WrapperComponent";

function ChildrenPage() {
  return (
    <div>
      <WrapperComponent>
        <div className="my-6 p-6 bg-green-600">
          <h1 className="text-white text-center text-3xl">
            Ini tulisan saat pemanggilan wrapper component pertama
          </h1>
        </div>
      </WrapperComponent>
      <WrapperComponent>
        <div className="my-6 p-6 bg-blue-600">
          <h1 className="text-white text-center text-3xl">
            Ini tulisan saat pemanggilan wrapper component kedua
          </h1>
        </div>
      </WrapperComponent>
      <WrapperComponent>
        <div className="my-12 p-16 bg-purple-600">
          <h1 className="text-white text-center text-6xl">
            Ini tulisan saat pemanggilan wrapper component ketiga
          </h1>
        </div>
      </WrapperComponent>
    </div>
  );
}

export default ChildrenPage;
