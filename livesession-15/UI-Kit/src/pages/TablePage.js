import React, { useContext, useEffect } from "react";
import { Helmet } from "react-helmet";
import TableArticle from "../components/TableArticle";
import { GlobalContext } from "../context/GlobalContext";
import Layout from "../layout/Layout";

function TablePage() {
  const { articles, fetchArticles, loading } = useContext(GlobalContext);

  useEffect(() => {
    // console.log("Fetch Data.....");
    fetchArticles();
  }, []);

  return (
    <div>
      {/* NAVBAR */}
      <Helmet>
        <title>Table Article Page</title>
      </Helmet>
      <Layout>
        <TableArticle />
      </Layout>
    </div>
  );
}

export default TablePage;
