import React from "react";
import { Helmet } from "react-helmet";
import CreateForm from "../components/CreateForm";
import Layout from "../layout/Layout";

function CreatePage() {
  return (
    <div>
      {/* NAVBAR */}
      <Helmet>
        <title>Create Article Page</title>
      </Helmet>
      <Layout>
        <CreateForm />
      </Layout>
    </div>
  );
}

export default CreatePage;
