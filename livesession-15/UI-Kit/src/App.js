import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import ChildrenPage from "./pages/ChildrenPage";
import CreatePage from "./pages/CreatePage";
import HomePage from "./pages/HomePage";
import TablePage from "./pages/TablePage";
import UpdatePage from "./pages/UpdatePage";

function App() {
  return (
    <div className="">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/table" element={<TablePage />} />
          <Route path="/create" element={<CreatePage />} />
          <Route path="/update/:articleId" element={<UpdatePage />} />
          <Route path="/children-props" element={<ChildrenPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
