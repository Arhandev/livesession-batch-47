import React from "react";
import { Navigate, Outlet } from "react-router-dom";

// mencegah user yang belum login supaya ke direct ke halaman login
function ProtectedRoute() {
  // belum login
  if (localStorage.getItem("token") == null) {
    return <Navigate to="/login" replace />;
  }

  return <Outlet />;
}

export default ProtectedRoute;
