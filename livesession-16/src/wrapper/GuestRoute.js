import React from "react";
import { Navigate, Outlet } from "react-router-dom";

// mencegah user yang belum login supaya ke direct ke halaman login
function GuestRoute() {
  // sudah login
  if (localStorage.getItem("token") != null) {
    return <Navigate to="/table" replace />;
  }

  return <Outlet />;
}

export default GuestRoute;
