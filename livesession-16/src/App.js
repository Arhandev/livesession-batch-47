import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import ChildrenPage from "./pages/ChildrenPage";
import CreatePage from "./pages/CreatePage";
import HomePage from "./pages/HomePage";
import LoginPage from "./pages/LoginPage";
import ProfilePage from "./pages/ProfilePage";
import RegisterPage from "./pages/RegisterPage";
import TablePage from "./pages/TablePage";
import UpdatePage from "./pages/UpdatePage";
import GuestRoute from "./wrapper/GuestRoute";
import ProtectedRoute from "./wrapper/ProtectedRoute";

function App() {
  return (
    <div className="">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<HomePage />} />

          <Route path="/children-props" element={<ChildrenPage />} />

          <Route element={<GuestRoute />}>
            <Route path="/register" element={<RegisterPage />} />
            <Route path="/login" element={<LoginPage />} />
          </Route>

          <Route element={<ProtectedRoute />}>
            <Route path="/profile" element={<ProfilePage />} />
            <Route path="/table" element={<TablePage />} />
            <Route path="/create" element={<CreatePage />} />
            <Route path="/update/:articleId" element={<UpdatePage />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
