import React from "react";
import { Helmet } from "react-helmet";
import LoginForm from "../components/LoginForm";
import Layout from "../layout/Layout";

function LoginPage() {
  return (
    <div>
      {/* NAVBAR */}
      <Helmet>
        <title>Login Page</title>
      </Helmet>
      <Layout>
        <LoginForm />
      </Layout>
    </div>
  );
}

export default LoginPage;
