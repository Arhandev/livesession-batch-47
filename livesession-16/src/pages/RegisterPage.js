
import React from "react";
import { Helmet } from "react-helmet";
import RegisterForm from "../components/RegisterForm";
import Layout from "../layout/Layout";

function RegisterPage() {
  return (
    <div>
      {/* NAVBAR */}
      <Helmet>
        <title>Register Page</title>
      </Helmet>
      <Layout>
        <RegisterForm />
      </Layout>
    </div>
  );
}

export default RegisterPage;
