import React from "react";
import UpdateForm from "../components/UpdateForm";
import Layout from "../layout/Layout";

function UpdatePage() {
  return (
    <div>
      {/* NAVBAR */}

      <Layout>
        <UpdateForm />
      </Layout>
    </div>
  );
}

export default UpdatePage;
