import { Button, Input } from "antd";
import axios from "axios";
import { useFormik } from "formik";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import * as Yup from "yup";

const validationSchema = Yup.object({
  name: Yup.string().required("Nama Pengguna wajib diisi"),
  username: Yup.string().required("Username Pengguna wajib diisi"),
  email: Yup.string()
    .required("Email Pengguna wajib diisi")
    .email("Format email tidak valid"),
  password: Yup.string().required("Password wajib diisi"),
  password_confirmation: Yup.string().required(
    "Konfirmasi Password wajib diisi"
  ),
});

function RegisterForm() {
  const navigate = useNavigate();
  const [input, setInput] = useState({
    name: "",
    username: "",
    email: "",
    password: "",
    password_confirmation: "",
  });

  const onSubmit = async (values) => {
    console.log(values);
    console.log("Menjalankan Post Request");
    try {
      // fetch data menggunakan axios
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/register",
        {
          name: values.name,
          username: values.username,
          email: values.email,
          password: values.password,
          password_confirmation: values.password_confirmation,
        }
      );

      alert("Berhasil registrasi");
      navigate("/verifikasi");
    } catch (e) {
      console.log(e);
      alert(e.response.data.info);
    }
  };

  const {
    handleChange,
    values,
    handleSubmit,
    errors,
    touched,
    handleBlur,
    setFieldValue,
    setFieldTouched,
  } = useFormik({
    initialValues: input,
    onSubmit: onSubmit,
    validationSchema: validationSchema,
  });

  return (
    <div>
      <section className="max-w-3xl mx-auto shadow-lg rounded-lg bg-white p-8 my-8">
        <h1 className="text-2xl text-cyan-500">Registrasi Pengguna</h1>
        <div className="grid grid-cols-5 gap-x-6 gap-y-4 my-2">
          <div className="col-span-5">
            <label className="block mb-1">Nama pengguna</label>
            <Input
              onChange={handleChange}
              name="name"
              type="text"
              placeholder="Masukkan Nama Pengguna"
              value={values.name}
              onBlur={handleBlur}
            />
            {touched.name === true && errors.name != null && (
              <p className="my-0 text-red-500 text-base">{errors.name}</p>
            )}
          </div>
          <div className="col-span-5">
            <label className="block mb-1"> Username</label>
            <Input
              onChange={handleChange}
              name="username"
              type="text"
              placeholder="Masukkan Username"
              value={values.username}
              onBlur={handleBlur}
            />
            {touched.username === true && errors.username != null && (
              <p className="my-0 text-red-500 text-base">{errors.username}</p>
            )}
          </div>
          <div className="col-span-5">
            <label className="block mb-1"> Email Pengguna</label>
            <Input
              onChange={handleChange}
              name="email"
              type="text"
              placeholder="Masukkan Email"
              value={values.email}
              onBlur={handleBlur}
            />
            {touched.email === true && errors.email != null && (
              <p className="my-0 text-red-500 text-base">{errors.email}</p>
            )}
          </div>
          <div className="col-span-5">
            <label className="block mb-1"> Password</label>
            <Input.Password
              onChange={handleChange}
              name="password"
              type="text"
              placeholder="Masukkan Password"
              value={values.password}
              onBlur={handleBlur}
            />
            {touched.password === true && errors.password != null && (
              <p className="my-0 text-red-500 text-base">{errors.password}</p>
            )}
          </div>
          <div className="col-span-5">
            <label className="block mb-1">Konfirmasi Password</label>
            <Input.Password
              onChange={handleChange}
              name="password_confirmation"
              type="text"
              placeholder="Masukkan Konfirmasi Password"
              value={values.password_confirmation}
              onBlur={handleBlur}
            />
            {touched.password_confirmation === true &&
              errors.password_confirmation != null && (
                <p className="my-0 text-red-500 text-base">
                  {errors.password_confirmation}
                </p>
              )}
          </div>
        </div>
        <div className="flex justify-center gap-4 my-4">
          <Button htmlType="submit" type="primary" onClick={handleSubmit}>
            Registrasi
          </Button>
        </div>
      </section>
    </div>
  );
}

export default RegisterForm;
