import { Button } from "antd";
import axios from "axios";
import React from "react";
import { Link, useNavigate } from "react-router-dom";

function Navbar() {
  const navigate = useNavigate();
  const onLogout = async () => {
    try {
      // Melakukan request Logout
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/logout",
        {},
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
    } catch (e) {
      console.log(e);
      alert(e.response.data.info);
    } finally {
      // Menghapus Local Storage
      localStorage.removeItem("token");
      localStorage.removeItem("username");
      alert("Berhasil Logout");
      navigate("/login");
    }
  };

  return (
    <header class="shadow-lg py-4 bg-white px-12 sticky z-10 top-0">
      <nav class="mx-auto max-w-7xl flex justify-between">
        <div></div>
        <ul class="flex items-center gap-6 text-xl list-none my-0">
          <Link className="text-cyan-500 no-underline" to="/">
            <li>Home</li>
          </Link>
          <Link className="text-cyan-500 no-underline" to="/table">
            <li>Table</li>
          </Link>
          <Link className="text-cyan-500 no-underline" to="/create">
            <li>Form</li>
          </Link>
        </ul>
        <div class="flex items-center gap-4">
          {localStorage.getItem("token") == null ? (
            <>
              <Link to="/login">
                <Button type="primary">Login</Button>
              </Link>

              <Link to="/register">
                <Button type="default">Register</Button>
              </Link>
            </>
          ) : (
            <>
              <Link to="/profile">
                <p className="text-lg text-cyan-500 my-0">
                  {localStorage.getItem("username")}
                </p>
              </Link>

              <Button onClick={onLogout} type="default" danger>
                Logout
              </Button>
            </>
          )}
        </div>
      </nav>
    </header>
  );
}

export default Navbar;
