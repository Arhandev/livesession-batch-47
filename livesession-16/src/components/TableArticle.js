import { Button, Table } from "antd";
import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { GlobalContext } from "../context/GlobalContext";

function TableArticle() {
  const { articles, fetchArticles } = useContext(GlobalContext);
  const [filter, setFilter] = useState({
    highlight: "",
    search: "",
  });
  const [filterData, setFilterData] = useState([]);

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
    },
    {
      title: "Nama Artikel",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Konten Artikel",
      dataIndex: "content",
      key: "content",
    },
    {
      title: "Image URL",
      dataIndex: "image_url",
      key: "image_url",
      render: (text, record, index) => {
        return <img className="w-64" src={record.image_url} alt="" />;
      },
    },
    {
      title: "Highlight",
      dataIndex: "highlight",
      key: "highlight",
      render: (text, record, index) => {
        if (record.highlight === true) {
          return <p>Aktif</p>;
        } else {
          return <p>Tidak Aktif</p>;
        }
      },
    },
    {
      title: "Action",
      dataIndex: "action",
      key: "action",
      render: (text, record, index) => {
        return (
          <div className="flex gap-2">
            <Link to={`/update/${record.id}`}>
              <Button type="primary">Update</Button>
            </Link>
            <Button
              type="primary"
              onClick={() => {
                //   console.log(record.id);
                onDelete(record.id);
              }}
              danger
            >
              Delete
            </Button>
          </div>
        );
      },
    },
  ];

  const handleChange = (e) => {
    if (e.target.name === "search") {
      setFilter({
        ...filter,
        search: e.target.value,
      });
    } else if (e.target.name === "highlight") {
      setFilter({
        ...filter,
        highlight: e.target.value,
      });
    }
  };

  const handleSearch = () => {
    // console.log(filter);
    let tmpArticles = structuredClone(articles);

    // jika search diisi
    if (filter.search !== "") {
      tmpArticles = tmpArticles.filter((article) => {
        return article.name.toLowerCase().includes(filter.search.toLowerCase());
      });
    }

    if (filter.highlight !== "") {
      tmpArticles = tmpArticles.filter((article) => {
        return article.highlight.toString() === filter.highlight;
      });
    }

    setFilterData(tmpArticles);
  };

  const handleReset = () => {
    setFilter({
      highlight: "",
      search: "",
    });
    setFilterData(articles);
  };

  const onDelete = async (id) => {
    try {
      // mengirimkan sebuah delete request
      const response = await axios.delete(
        `https://api-project.amandemy.co.id/api/articles/${id}`
      );
      fetchArticles();
      alert("Berhasil menghapus artikel");
    } catch (error) {
      alert("Terjadi Sesuatu Error");
    }
  };

  useEffect(() => {
    setFilterData(articles);
  }, [articles]);

  return (
    <section>
      <h1 className="my-8 text-3xl font-bold text-center">Table Article</h1>

      <div className="max-w-6xl mx-auto w-full my-4">
        <div className="mb-6 flex gap-3 justify-end">
          <select
            onChange={handleChange}
            value={filter.highlight}
            name="highlight"
            id=""
          >
            <option value={""}>Pilih Filter Highlight</option>
            <option value="true">Aktif</option>
            <option value="false">Tidak Aktif</option>
          </select>

          <input
            onChange={handleChange}
            value={filter.search}
            name="search"
            placeholder="Search article name"
            type="text"
          />

          <Button onClick={handleSearch} type="primary">
            Search
          </Button>
          <Button onClick={handleReset} type="primary" danger>
            Reset
          </Button>
        </div>
        <Table dataSource={filterData} columns={columns} />
      </div>
    </section>
  );
}

export default TableArticle;
