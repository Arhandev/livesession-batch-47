import React from "react";
import ThirdComponent from "./ThirdComponent";

function SecondComponent({ kalimat }) {
  return (
    <div className="m-4 p-4 bg-green-600 text-white">
      <h1 className="text-lg">Second Component</h1>
      <ThirdComponent kalimat={kalimat} />
    </div>
  );
}

export default SecondComponent;
