import React from "react";
import SecondComponent from "./SecondComponent";

function FirstComponent({ kalimat }) {
  return (
    <div className="m-4 p-4 bg-red-600 text-white">
      <h1 className="text-lg">First Component</h1>
      <SecondComponent kalimat={kalimat} />
    </div>
  );
}

export default FirstComponent;
