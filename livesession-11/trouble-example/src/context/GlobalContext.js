import { createContext, useState } from "react";

export const GlobalContext = createContext();

export const GlobalProvider = ({ children }) => {
  const kalimat = "Farhan Abdul Hamid";

  const [text, setText] = useState("Halo Dunia");

  const changeText = () => {
    setText("Hello World!");
  };

  return (
    <GlobalContext.Provider
      value={{
        kalimat: kalimat,
        text: text,
        setText: setText,
        changeText: changeText,
      }}
    >
      {children}
    </GlobalContext.Provider>
  );
};
