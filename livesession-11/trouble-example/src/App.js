import { useContext } from "react";
import "./App.css";
import FirstComponent from "./components/FirstComponent";
import { GlobalContext } from "./context/GlobalContext";

function App() {
  const { kalimat, changeText } = useContext(GlobalContext);
  return (
    <div className="m-4 p-4 bg-orange-500 text-white">
      <h1>App Component</h1>
      <h1>{kalimat}</h1>
      <button
        onClick={changeText}
        className="bg-black text-white px-8 py-4 text-lg"
      >
        Klik Aku
      </button>
      <FirstComponent kalimat={kalimat} />
    </div>
  );
}

export default App;
