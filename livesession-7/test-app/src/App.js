import "./App.css";
import Header from './components/Header.js'

function App() {
  return (
    <div className="App">
      <h1 className="text-6xl text-red-600 paragraph-1">Hello World</h1>
      <Header/>
      <Header/>
      <Header/>
    </div>
  );
}

export default App;
