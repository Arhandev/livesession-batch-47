var y = {
    negara: "Indonesia",
    kota: "Jakarta",
    alamat: "Jalan Mawar",
    nama_lengkap: "Farhan Abdul"
};

// ======= Pemanggilan Value ========
// Cara 1
// console.log(y.kota);

// Cara 2
// console.log(y["nama lengkap"]);


// ======= Assigning Value =======
// Cara 1
y.umur = 15
// Cara 2
// y['negara'] = "Malaysia"

delete y.kota


console.log(y);