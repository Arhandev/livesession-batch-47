// Tanpa Parameter
// function print() {
//   console.log("Pemanggilan Function Berhasil");
//   console.log("Function Selesai");
// }
// print();

// Dengan Parameter
// function print(x, y, z) {
//   console.log(x);
//   console.log(y);
//   console.log(z);
// }
// print("Hello World", "Nama Saya", "Farhan");

// Dengan Return
// function luasPersegi(panjang, lebar) {
// //   var result = panjang * lebar;
//   return panjang * lebar;
// }

// var hasilLuas = luasPersegi(30, 20);

// console.log(hasilLuas);

// function dalam function

function print(x) {
  console.log(x);
}

function showNumber() {
  for (var i = 1; i <= 10; i++) {
    print(i); // memanggil function print dalam showNumber
  }
}

showNumber();
