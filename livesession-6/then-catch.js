const promiseFunction = () => {
  return new Promise((resolve, rejected) => {
    console.log("Promise Berjalan");
    // resolve("Promise berhasil dijalankan"); // misal: data dari server
    rejected("Promise gagal dijalankan"); // server error
  });
};

// then untuk mendapatkan data dari server
promiseFunction()
  .then((response) => {
    console.log(response);
  })
  .catch((error) => {
    console.log(error);
  });
