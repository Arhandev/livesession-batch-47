const promiseFunction = () => {
  return new Promise((resolve, rejected) => {
    console.log("Promise Berjalan");
    resolve("Promise berhasil dijalankan"); // misal: data dari server
    // rejected("Promise gagal dijalankan"); // server error
  });
};

const executePromise = async () => {
  try {
    const result = await promiseFunction();
    console.log(result);
  } catch (error) {
    console.log("Ada error :  ");
    console.log(error);
  }
};

executePromise();
