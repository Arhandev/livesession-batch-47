import axios from "axios";
import { createContext, useState } from "react";

export const GlobalContext = createContext();

export function ContextProvider({ children }) {
  const [articles, setArticles] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchArticles = async () => {
    // menandai bahwa aplikasi melakukan fetch
    setLoading(true);
    try {
      // fetch data menggunakan axios
      const response = await axios.get(
        "https://api-project.amandemy.co.id/api/articles"
      );
      // console.log(response.data.data); // berhasil mengakses isi artikel
      setArticles(response.data.data); // berhasil menyimpan artikel pada state
      // articles sudah terisi data dari server
    } catch (error) {
      alert("Terjadi Sesuatu Error");
    } finally {
      setLoading(false); // sukses atau error bakalan berjalan
    }
  };

  return (
    <GlobalContext.Provider
      value={{
        articles: articles,
        setArticles: setArticles,
        fetchArticles: fetchArticles,
        loading: loading,
        setLoading: setLoading,
      }}
    >
      {children}
    </GlobalContext.Provider>
  );
}
