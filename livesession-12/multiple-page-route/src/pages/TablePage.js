import React, { useContext, useEffect } from "react";
import Navbar from "../components/Navbar";
import Table from "../components/Table"
import { GlobalContext } from "../context/GlobalContext";

function TablePage() {

   const { articles, fetchArticles, loading } = useContext(GlobalContext);

   useEffect(() => {
     // console.log("Fetch Data.....");
     fetchArticles();
   }, []);


  return (
    <div>
      {/* NAVBAR */}
      <Navbar />
      <Table />
    </div>
  );
}

export default TablePage;
