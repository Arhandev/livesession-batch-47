import React from "react";
import CreateForm from "../components/CreateForm";
import Navbar from "../components/Navbar";

function CreatePage() {
  return (
    <div>
      {/* NAVBAR */}
      <Navbar />
      <CreateForm />
    </div>
  );
}

export default CreatePage;
