import React from "react";
import Navbar from "../components/Navbar";

function SecondPage() {
  return (
    <div>
      <Navbar />
      <h1 className="text-center text-4xl font-bold my-16">Halaman Kedua</h1>
    </div>
  );
}

export default SecondPage;
