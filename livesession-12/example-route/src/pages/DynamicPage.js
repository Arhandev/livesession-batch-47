import React from "react";
import { useParams } from "react-router-dom";
import Navbar from "../components/Navbar";

function DynamicPage() {
  const { id } = useParams();

  // sebuah fetch data

  return (
    <div>
      <Navbar />
      <h1 className="text-center text-6xl text-red-600 font-bold my-16">
        Halaman Dinamis
      </h1>
      <h1 className="text-center text-3xl text-red-600 font-bold my-16">
        mengambil data product dengan id {id}
      </h1>
    </div>
  );
}

export default DynamicPage;
